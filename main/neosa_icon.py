#!/usr/bin/python2
# -*- coding: utf-8 -*-

import gtk
import os
import sys
from log import l
import config

class NeosaIcon:
    def __init__(self):
        self.statusicon = gtk.StatusIcon()
        self.statusicon.set_from_file("./main/logos/logo-32.png") 
        self.statusicon.connect("popup-menu", self.right_click_event)
        self.statusicon.set_tooltip("Neosa - Neosa es otro sistema de autenticación")
        self.statusicon.set_title("Neosa")
        self.window = gtk.Window()
        
    def mostrar(self):
        gtk.main()
        
        
    def right_click_event(self, icon, button, time):
        menu = gtk.Menu()
        about = gtk.MenuItem("Sobre...")
        salir = gtk.MenuItem("Salir")
        block = gtk.MenuItem("Bloquear")
        sep = gtk.SeparatorMenuItem()
        setup = gtk.MenuItem("Configurar")
        
        about.connect("activate", self.show_about_dialog)
        salir.connect("activate", self.main_quit)
        block.connect("activate", self.block_host)
        setup.connect("activate", self.setup_devices)
        
        menu.append(block)
        menu.append(sep)
        menu.append(setup)
        menu.append(about)
        menu.append(salir)
        menu.show_all()
        menu.popup(None, None, gtk.status_icon_position_menu, button, time, self.statusicon)
        
    def show_about_dialog(self, widget):
        about_dialog = gtk.AboutDialog()
        about_dialog.set_destroy_with_parent(True)
        about_dialog.set_name("Neosa - Sistema de autenticación")
        about_dialog.set_version("1.0")
        about_dialog.set_authors(["Matías Varela","---", "Universidad de Mendoza", "Facultad de Ingeniería", "2013"])
        about_dialog.run()
        about_dialog.destroy()
        
    def block_host(self, widget):
        self.conf = config.NeosaConfig()
        if self.conf.any():
            l.info("Llamando a block_host...")
            import subprocess
            subprocess.Popen((sys.executable, os.path.abspath("main/block_host.py")))
        else:
            print "No hay dispositivos asociados."
            d = gtk.Dialog( title="Neosa Server: ERROR",
                            parent=self.window,
                            flags=gtk.DIALOG_DESTROY_WITH_PARENT,
                            buttons=("Sí",True, "No", False))
            label = gtk.Label()
            text =  "<b>No hay dispositivos asociados.</b>\n\n¿Desea abrir la consola de configuración?"
            label.set_markup(text)
            label.set_padding( 30, 30)
            d.vbox.pack_start(label)
            label.show()
            result = d.run()
            if result:
                d.destroy()
                self.setup_devices(widget) 
            else:
                d.destroy()
            
    def setup_devices(self,widget):
        # import vte
        try:
            import vte
            v = vte.Terminal()
            v.connect ("child-exited", lambda term: self.restart_service())
            v.connect ("contents-changed", lambda v: self.is_finish(v))
            v.fork_command()
            v.feed_child('bash setup.sh\n')
            self.window.add(v)
            self.window.connect('delete-event', lambda window, event: self.restart_service())
            self.window.show_all()
            gtk.main()
            
            self.restart_service()
        except:
            error = gtk.MessageDialog (None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_OK, 
                                       'Es necesario instalar libvte para python.')
            error.run()
            gtk.main_quit()


    def is_finish(self, v):
        aux = repr(v.get_text(lambda *a: True).rstrip())
        if aux.find("Exit") != -1:
            self.restart_service()
        if aux.find("^C") != -1:
            self.restart_service()
        #if aux.find("Traceback") != -1:
        #    self.restart_service()
            
    def main_quit(self, widget):
        from pid import kill_process_by_pid
        kill_process_by_pid("neosa")
        
        l.info("Buena suerte!")
        gtk.main_quit()
        #gtk.main_quit()       
        sys.exit(0)
        
    def restart_service(self):
        from pid import kill_process_by_pid
        kill_process_by_pid("neosa")
        import subprocess
        subprocess.Popen((sys.executable, os.path.abspath("main/neosa_service.py")))
        self.window.destroy()