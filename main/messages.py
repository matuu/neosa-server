# -*- coding: utf-8 -*-

#De estado
ERROR = -1
OK = 200
#De solicitud
INIT = 100
UUID_PASS = 1000
MAC_DEV = 2000
#De resultado
NOT_AUTH = 501
AUTHENTICATED = 502
NOT_ASSOC = 401
ASSOCIATED = 402
#Acciones
ASSOCIATE = 400
AUTHENTICATE = 500
#Mensajes de resultados
MSG_AUTH_OK = 10
MSG_ASSOC_OK = 11
MSG_ERROR_CONN=12
MSG_NOT_UNPROT=13
MSG_NOT_ASSOC=14
MSG_NOT_IS_ASSOC=15
MSG_NOT_IS_AUTH=16
MSG_NOTHING=17
MSG_ERROR=0


class Messages:
    def __init__(self):
        #Miembros del mensaje
        self.num_msj=0
        self.msj=""
        self.code=0
        
    def set_mensaje_Snd(self,msg,cod):
        self.msj = msg
        self.code = cod
        
    def set_mensaje_Recv(self,num,msg,cod):
        self.num_msj = num
        self.msj = msg
        self.code = cod 
        
    def set_from_str(self,string):
        try:
            memb = string.split(';')
            if len(memb) != 3:
                self.code = ERROR
                self.msj = "Cadena sin formato"
            else:
                self.num_msj = int(memb[0])
                self.msj = memb[1]
                self.code = int(memb[2])
        except ValueError:
            self.code = ERROR
            self.msj = "No es un numero valido."
        except AttributeError:
            self.code = ERROR
            self.msj = "No es una cadena valida."
        
    def is_error(self):
        if self.code == ERROR:
            return True
        else:
            return False
        
    def is_valid(self, num_sec):
        if self.num_msj == num_sec:
            return True
        else:
            return False
        
    def __str__(self, num_sec):
        return "%s;%s;%s" % (num_sec, self.msj, self.code)
                
    def __getattribute__(self, attr):
        return object.__getattribute__(self, attr)
    