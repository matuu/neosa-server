# -*- coding: utf-8 -*-
import logging
logging.basicConfig(filename='neosa.log', level=logging.DEBUG, format='%(asctime)s [%(levelname)s] – %(threadName)-10s : %(message)s')

l = logging.getLogger("neosa_service")