# -*- coding: utf-8 -*-
from device_auth import Device_Auth
from xml.sax.handler import ContentHandler
import xml.sax
from xml.dom import minidom
import os
from log import l

class NeosaConfig:
    
    def __init__(self):
        #list de dispositivos cargados desde el xml
        self.devices_auths = []
        #Creamos el parser
        xml_analizador = xml.sax.make_parser()
        #le asignamos un manejador
        xml_analizador.setContentHandler(ContentHandler())
        #URL del archivo de configuracion
        
        config_file = os.path.realpath("main/config/config_neosa.xml")
        l.info(config_file)
        #expanduser("~/."+file_name+".lock")  #"./config/config_neosa.xml"
        try:
            #Comprobamos la estructura del xml
            xml_analizador.parse(config_file)
            # Cargamos en objeto arbol_dom el documento xml
            self.arbol_dom = minidom.parse(config_file)
            # traemos los nodos
            self.nodos = self.arbol_dom.documentElement #childNodes
            # Accedemos al primer nodo: auth_mobile
            self.auth_mobile = self.nodos.getElementsByTagName("auth_mobile")[0]           
            self.cargarDevs()
            #for x in lista_aux:
            #    dev = Device_Auth()
            #    dev.set_device(x.getAttribute("mac"), x.getAttribute("name"), x.getAttribute("uuidpass"))
            #    self.devices_auths.append(dev)
        except Exception, err:
            print "Error %s:\n\t %s esta corrupto" % (err, config_file)
        
    def cargarDevs(self):
        self.devices_auths = []
        # Nos traemos la lista de mobiles autorizados
        lista_aux = self.auth_mobile.getElementsByTagName("mobile")
        # Creamos un list de objetos device_auth
        for x in lista_aux:
            dev = Device_Auth()
            dev.set_device(x.getAttribute("mac"), x.getAttribute("name"), x.getAttribute("uuidpass"))
            self.devices_auths.append(dev)
        
    def any(self):
        for x in self.devices_auths:
            return True
        return False
    
    def mostrarXML(self):
        #Funcion que imprime el archivo xml de configuración
        print "Dispositivos autorizados:"
        
        lista_aux = self.auth_mobile.getElementsByTagName("mobile")
        for x in lista_aux:
            print " " + x.getAttribute("name"),
            print "(" + x.getAttribute("mac") +")"
            #print " " + x.getAttribute("uuidpass")

    def getDev4Index(self,idx):
        try:
            nodo = self.devices_auths[int(idx)]
            print " ", nodo
            return True
        except IndexError:
            print "ERROR: Opción incorrecta."
            return False
    
    def removeDev(self, idx):
        self.auth_mobile.removeChild(self.auth_mobile.getElementsByTagName("mobile")[idx])
        self.guardar_xml()
        #self.cargarDevs()
        print "Configuración guardada."
        #self.mostrarXML()
        
    def addDev(self, dev):
        x = self.arbol_dom.createElement("mobile")
        x.setAttribute("mac", dev.mac)
        x.setAttribute("name", dev.name)
        x.setAttribute("uuidpass", dev.uuidpass)
        #si existe auth_device con esta mac, reemplazamos
        
        exist = self.getDevAuth(dev.mac)
        if exist is not None:
            print "Existe un dispositivo con esa MAC, removiendo entrada..."
            i = 0
            for l in self.devices_auths:
                if l.mac == dev.mac:
                    self.removeDev(i)
                i+=1
        print "Añadiendo dispositivo..."
        self.auth_mobile.appendChild(x)  
        self.guardar_xml()
        print "Configuración guardada."
                
    def is_autorizado(self,host):
        #Funcion que comprueba si el movil está autorizado a usar el servicio
        print "Comprobando", host
        dev = self.getDevAuth(host)
        if dev is not None:
            print "OK. Móvil ", dev.name, " encontrado" 
            return True
        return False
            
    def print_list(self):
        i = 0
        for l in self.devices_auths:
            i+=1
            print i, "- ",l
            
    def getDevAuth(self,mac):
        #devolver device_auth conectado
        for l in self.devices_auths:
            if l.mac == mac:
                return l
        return None
    
    
    def search(self, key, by="mac"):
        for index, dev in enumerate(self.devices_auths):
            if dev.__getattribute__(by) == key:
                return index
        return None

        
    def guardar_xml(self):
        # Abrimos fichero en donde guardar el documento XML.
        fichero = open(os.path.realpath("main/config/config_neosa.xml"), "w")

        # Escribimos con writexml en la cabecera la codificación 'iso-8859-1', para
        # no tener problemas con tildes y demás. Aparecerá así:
        # <?xml version="1.0" encoding="iso-8859-1"?>
        self.arbol_dom.writexml(fichero, encoding='iso-8859-1')

        # Finalmente cerramos el fichero.
        fichero.close()