#!/usr/bin/python2
# -*- coding: utf-8 -*-

import bluetooth
import sys
import config
import pid
from log import l
import messages as msj
from device_auth import Device_Auth

name = "Neosa"
uuid = "00001101-0000-1000-8000-00805F9B34FB"


class Setup:
    
    def __init__(self):
        print "Acciones disponibles:"
        print "\t1) Añadir un dispositivo autorizado."
        print "\t2) Ver o quitar un dispositivo autorizado."
        print "\tq) Salir."
        
        hayOpcion = False
        while hayOpcion is False:
            opcion = raw_input("\nOpción: ")
            if(opcion == "q"):
                print "Exit!"
                sys.exit(0)
            try:
                inp = int(opcion)
            except:
                continue
            if(1 == inp):
                print "Parando el servicio neosa...",
                               
                pid.kill_process_by_pid("neosa")
                print "OK"
                print "En el dispositivo, ejecute Neosa Client, luego en Preferencias, pulse el botón <<Registrar dispositivo>>."
                self.server_sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
                self.server_sock.bind(("",2))
                self.server_sock.listen(1)
                bluetooth.advertise_service( self.server_sock, "NeosaConfig",
                       service_id = uuid,  
                       service_classes = [ bluetooth.SERIAL_PORT_CLASS ], 
                       profiles = [ bluetooth.SERIAL_PORT_PROFILE ] )
                print "Esperando... (Presione Ctrl + C para salir)"
                self.dev = Device_Auth()
                self.sock, client_info = self.server_sock.accept()
                self.dev.mac = client_info[0]
                
                num_sec = 0
                isError = False
                isSuccess = False
                
                data = self.sock.recv(1024)
                l.debug("<- " + str(data))
                if(int(data) == 1):
                    msjStr = "Autenticación requerida. Debe finalizar el módulo de configuración antes de intenter desproteger el equipo"
                    print msjStr
                    mError = msj.Messages()
                    mError.set_mensaje_Snd("Error: "+ msj, msj.MSG_NOT_IS_ASSOC)
                    self.sock.send(mError.__str__(num_sec),1024)
                    print "Exit!"
                    sys.exit(0)
                elif (int(data) == 2):
                    print "Asociación requerida."
                    
                #INIT
                msg_data = msj.Messages()
                msg_data.set_mensaje_Snd("Iniciar", msj.INIT)
                bInit = self.sock.send(msg_data.__str__(num_sec),1024)                
                l.debug("-> " + str(bInit))
                while (isError is not True) & (isSuccess is not True):
                    num_sec+=1
                    data = self.sock.recv(1024)
                    msg_data.set_from_str(data)
                    l.debug( "<- "+ data)
                    if msg_data.is_error() is not True & msg_data.is_valid(num_sec): 
                                            
                        if msg_data.code == msj.OK:
                            #se debe recibir un OK desde el cliente
                            num_sec+=1
                            mUuid = msj.Messages()
                            mUuid.set_mensaje_Snd("UUID+PASS requerido.", msj.UUID_PASS)
                            sUuid = self.sock.send(mUuid.__str__(num_sec),1024)
                            l.debug( "-> "+ str(sUuid))
                        elif msg_data.code == msj.UUID_PASS:
                            #Recibo el UUID+PASS del dispositivo. 
                            self.dev.uuidpass = msg_data.msj                           
                            l.debug( "UUID+PASS: "+msg_data.msj)
                            num_sec+=1
                            mAuth = msj.Messages()
                            mAuth.set_mensaje_Snd("Autenticación Satisfactoria", msj.ASSOCIATED)
                            sAuth= self.sock.send(mAuth.__str__(num_sec),1024)
                            l.debug( "->"+ str(sAuth))
                            
                        elif msg_data.code == msj.ASSOCIATED:
                            l.debug( "OK. Asociado. :-)")
                            isSuccess = True
                        elif msg_data.code == msj.NOT_ASSOC:
                            l.debug( "No Asociado. :-(")
                        elif msg_data.code == msj.ERROR:
                            l.debug( "Error en cliente")
                            isError = True
                            
                        if(isError):
                            num_sec+=1 
                            mError = msj.Messages()
                            mError.set_mensaje_Snd("No asociado O Error", msj.NOT_AUTH)
                            self.sock.send(mError.__str__(num_sec),1024)
                        
                    else:
                        isError = True
                if isError is True:
                    print "Error al asociar el dispositivo. Intente nuevamente."
                    raw_input("Presione un tecla para salir...")
                    print "Exit!"
                    return None
                if isSuccess is True:
                    self.dev.name = raw_input("Ingrese el nombre para este dispositivo: ")
                    conf = config.NeosaConfig()
                    conf.addDev(self.dev)
                    print "Parando módulo de configuración...",
                    bluetooth.stop_advertising(self.server_sock)
                    self.server_sock.close()
                    raw_input("Ok\nPresione una tecla para continuar")
                    print "Exit!"
                    hayOpcion=True
            elif(inp == 2):
                conf = config.NeosaConfig()
                rmDev = 0
                idx = 0
                while rmDev == 0:
                    print "Dispositivos asociados previamente: "
                    conf.print_list()
                    
                    try:
                        idx = raw_input("Dispositivo a desasociar ('q' para salir): ")
                        idx = int(idx) -1
                        if conf.getDev4Index(idx):
                            resp = raw_input("¿Seguro (s/N)? (q: Salir)")
                            if resp == "s":
                                rmDev = 1
                            elif resp == "q":
                                print "Exit!"
                                sys.exit(0)
                    except:
                        if idx == "q":
                            print "Exit!"
                            sys.exit(0)
                    
                conf.removeDev(idx)
                raw_input("Presione un tecla para salir...")
                print "Exit!"
                hayOpcion=True
            
            else:
                print "¡Opción inválida!"

            
if __name__ == "__main__":
    s = Setup()