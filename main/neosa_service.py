#!/usr/bin/python2
# -*- coding: utf-8 -*-

import bluetooth
import config
from receiver import NeosaReceiverThread
from log import l


name = "Neosa"
uuid = "00001101-0000-1000-8000-00805F9B34FB"

class NeosaService:
    
    def main(self):
        
        self.conf = config.NeosaConfig()
        l.info( "Iniciando el servicio...")
        self.server_sock=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.server_sock.bind(("",1))
        self.server_sock.listen(1)
        bluetooth.advertise_service( self.server_sock, name,
                   service_id = uuid,  
                   service_classes = [ bluetooth.SERIAL_PORT_CLASS ], 
                   profiles = [ bluetooth.SERIAL_PORT_PROFILE ] )
        l.info( "Iniciado. Esperando...")
        while True:         
            client_sock, client_info = self.server_sock.accept()
            
            l.info(str(client_info)+ ": Conexión entrante. Autorizando...")
            
            if self.conf.is_autorizado(client_info[0]):
                dev_auth = self.conf.getDevAuth(client_info[0])
                #Al conectar, creo un hilo para manejar la conexión y autenticación
                echo = NeosaReceiverThread(client_sock, dev_auth)
                echo.setDaemon(True)
                echo.start()
            else:
                l.warn( "Movil no autorizado. :(")
                client_sock.close()
        self.server_sock.close()
        
    

if __name__ == "__main__":
    import pid
    import sys
    #print "Iniciando sistema Neosa"
    pid.kill_process_by_pid("neosa")
    if(pid.pid_save("neosa")):
        if len(sys.argv) == 1:
            service = NeosaService()
            service.main()
            
        elif len(sys.argv) == 2:
            if sys.argv[1] == "--setup":
                service.abmDeviceAuthorized()
            else:
                print "Uso: neosa_service [--setup]"
                print "./neosa_service.py: Iniciar el servicio.\n./neosa-service.py --setup: Configurar dispositivos autorizados."
                sys.exit(2)
