# -*- coding: utf-8 -*-
#/usr/bin/env python
import os
import signal
from log import l
def check_service(file_name):
    """ Chequea que no se este ejecutando el servicio """
    if os.access(os.path.expanduser("~/."+file_name+".lock"), os.F_OK):
        #si el lockfile existe, se chequea el numero PID
        #en el archivo de bloqueo
        pidfile = open(os.path.expanduser("~/."+file_name+".lock"), "r")
        pidfile.seek(0)
        old_pid = pidfile.readline()
        # Ahora checamos que el PID del lockfile se corresponde con el PID del proceso
        if os.path.exists("/proc/%s" % old_pid):
            l.warn( "Existe una instancia del proceso ejecutandose")
            l.warn ("Este es el proceso %s," + old_pid)
            return False
        else:
            l.warn ( "El archivo está ahí, pero el programa no se está ejecutando")
            l.warn ( "Removiendo el archivo de bloqueo del proceso " + old_pid)
            os.remove(os.path.expanduser("~/."+file_name+".lock"))
            return True
    return True

def pid_save(file_name):
    
    try:
        pid = str(os.getpid())
        l.info( "PID del proceso: "+pid)
        l.info( "Guardando PID en " + str(os.path.expanduser("~/."+file_name+".lock")))
        f = open(os.path.expanduser("~/."+file_name+".lock"), "wt")
        f.write(pid)
        f.close()
        return True
    except IOError:
        l.error( "Error: no se pudo escribir el PID" )
        return False
    
    
def pid_remove(file_name):
    if os.access(os.path.expanduser("~/."+file_name+".lock"), os.F_OK):
        os.remove(os.path.expanduser("~/."+file_name+".lock"))
        return True
    else:
        l.error( "No se encontro ~/." + file_name+".lock, no eliminado. ")
        return False
    
def pid_exists(file_name):
    if os.access(os.path.expanduser("~/."+file_name+".lock"), os.F_OK):
        return True
    return False

def kill_process_by_pid(file_name):
    try:
        l.info( "Comprobando archivo "+ os.path.expanduser("~/."+file_name+".lock"))
        if os.access(os.path.expanduser("~/."+file_name+".lock"), os.F_OK):
            pidfile = open(os.path.expanduser("~/."+file_name+".lock"), "r")
            pidfile.seek(0)
            pid = pidfile.readline()
            if os.path.exists("/proc/%s" % pid):
                l.info( "Proceso localizado, enviando señal KILL.")
                os.kill(int(pid), signal.SIGKILL)
                l.info( "Eliminando archivo lock.")
                os.remove(os.path.expanduser("~/."+file_name+".lock"))
                return True
            else:
                l.info( "Archivo lock eliminado, aunque el proceso no se encontraba en ejecución.")
                os.remove(os.path.expanduser("~/."+file_name+".lock"))
                return True
        else:
            l.warn( "No existe el pidfile en "+ file_name)
            return False
    except OSError:
        l.error( "No exite el proceso con PID "+ pid)
        return False