# -*- coding: utf-8 -*-
import threading
import messages as msj
from log import l
from device_auth import Device_Auth
from pid import kill_process_by_pid, pid_exists

class NeosaReceiverThread(threading.Thread):
    def __init__ (self,sock,dev_auth):
        threading.Thread.__init__(self)
        self.sock = sock
        self.client_dev = Device_Auth()
        self.client_dev = dev_auth
                
    def run(self):
        isSuccess = False
        isError = False
        num_sec = 0
                
        
        try:
            data = self.sock.recv(1024)
            if(int(data) == 1):
                l.info( "Autenticación requerida.")
            elif (int(data) == 2):
                print "Asociación requerida. Debe iniciar el servicio de configuración primero."
                mError = msj.Messages()
                mError.set_mensaje_Snd("Error: inicie el módulo de configuración para asociar un dispositivo", msj.MSG_NOT_IS_AUTH)
                self.sock.send(mError.__str__(num_sec),1024)
                return None
            #Comprobamos que el host este bloqueado, 
            # checando que este su lockfile
            if(pid_exists("blockhost")is not True):
                l.info( "Host desprotegido, nada que hacer.")
                mError = msj.Messages()
                mError.set_mensaje_Snd("Host desprotegido, nada que hacer.", msj.MSG_NOTHING)
                self.sock.send(mError.__str__(num_sec),1024)
                self.sock.close()
                return None
                
            #INIT
            msg_data = msj.Messages()
            msg_data.set_mensaje_Snd("Iniciar", msj.INIT)
            bInit = self.sock.send(msg_data.__str__(num_sec),1024)
                
            l.debug("-> " + str(bInit))
            bit_chk = 0
            while (isError is not True) & (isSuccess is not True):
                num_sec+=1
                data = self.sock.recv(1024)
                msg_data.set_from_str(data)
                l.debug( "<- "+ data)
                if msg_data.is_error() is not True & msg_data.is_valid(num_sec): 
                    #En cada caso, se solicita el dato siguiente
                    #l.debug( "code: " + str(msg_data.code))
                    
                    if msg_data.code == msj.OK:
                        #se debe recibir un OK desde el cliente
                        bit_chk+=1
                        num_sec+=1
                        mUuid = msj.Messages()
                        mUuid.set_mensaje_Snd("UUID requerido.", msj.UUID_PASS)
                        sUuid = self.sock.send(mUuid.__str__(num_sec),1024)
                        l.debug(str(sUuid)+ "-> "+ mUuid.__str__(num_sec))
                    elif msg_data.code == msj.UUID_PASS:
                        #Recibo el UUID del dispositivo. Debo validar si es un dispositivo autorizado
                        if self.client_dev.uuidpass == msg_data.msj:
                            bit_chk+=2
                            l.debug( "OK. UUID valido: "+msg_data.msj)
                            num_sec+=1
                            mMac = msj.Messages()
                            mMac.set_mensaje_Snd("MAC requerida", msj.MAC_DEV)
                            sMac = self.sock.send(mMac.__str__(num_sec),1024)
                            l.debug(str(sMac)+ "->"+ mMac.__str__(num_sec))
                        else:
                            l.debug( "UUID no válido.")
                            isError = True 
                    elif msg_data.code == msj.MAC_DEV:
                        l.debug(str("MAC cl "+ self.client_dev.mac+ " MAC recv: "+ msg_data.msj))
                        #Recibo la mac del dispositivo. Valido que sea la registrada.
                        if self.client_dev.mac == msg_data.msj:
                            bit_chk+=4
                            l.debug( "OK. Mac válida: "+ msg_data.msj)
                            num_sec+=1
                            mAuth = msj.Messages()
                            mAuth.set_mensaje_Snd("Autenticación Satisfactoria", msj.AUTHENTICATED)
                            sAuth= self.sock.send(mAuth.__str__(num_sec),1024)
                            l.debug(str(sAuth)+ "->"+ mAuth.__str__(num_sec))
                        else:
                            l.debug( "Mac no válida")
                            isError = True
                    elif msg_data.code == msj.AUTHENTICATED:
                        l.debug( "OK. Autorizado. :-)")
                        
                        if bit_chk==7: #es 7 si paso por las etapas anteriores
                            isSuccess = True
                    elif msg_data.code == msj.NOT_AUTH:
                        l.debug( "No autorizado. :-(")
                    elif msg_data.code == msj.ERROR:
                        l.debug( "Error en cliente")
                        isError = True
                        
                    if(isError):
                        num_sec+=1
                        
                        mError = msj.Messages()
                        mError.set_mensaje_Snd("No autenticado O Error", msj.NOT_AUTH)
                        self.sock.send(mError.__str__(num_sec),1024)
                    
                else:
                    isError = True

            if isSuccess is True and isError is not True:
                l.info( str("OK. Desproteger "+ self.client_dev.mac)) 
                pidfile = "blockhost"
                kill_process_by_pid(pidfile)
        except IOError:                                                                                     
            pass
        l.warn(str( self.client_dev.name+ ": Desconectanto!"))
        self.sock.close()