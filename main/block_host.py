#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#-------------------------
# Importación de módulos
#-------------------------

import pygame
from pygame.locals import KEYDOWN, K_ESCAPE, MOUSEMOTION
import pid
import sys
import os
import random
from log import l

#-------------------------
# Constantes
#-------------------------

SCREEN_WIDTH = 1366
SCREEN_HEIGHT = 768
IMG_DIR = "./main/img/"
TIME_BG = 3000

#------------------------
#Clases y funciones
#------------------------

def load_image(nombre, dir_imagen, alpha=False):
    #encontramos la ruta
    ruta = os.path.join(dir_imagen, nombre)

    try:
        image = pygame.image.load(ruta)
    except:
        l.error( "Error, no se puede cargar la imagen: "+ ruta)
        sys.exit(1)
    #comprobar si la imagen "canal alpha" (como los png)
    if alpha == True:
        image = image.convert_alpha()
    else:
        image = image.convert()
    return image

class Alerta(pygame.sprite.Sprite):
    
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = load_image("alerta.png", IMG_DIR, True)
        self.rect = self.image.get_rect()
        self.rect.centerx = (SCREEN_WIDTH / 2) 
        self.rect.centery = (SCREEN_HEIGHT / 2) 
        self.speed = [1, 1]
        
    def update(self):
        if self.rect.left < 0 or self.rect.right > SCREEN_WIDTH:
            self.speed[0] = -self.speed[0]
        if self.rect.top < 0 or self.rect.bottom > SCREEN_HEIGHT:
            self.speed[1] = -self.speed[1]
        self.rect.move_ip((self.speed[0], self.speed[1]))
        

#------------------
# Funcion principal
#------------------
class block_screen:
    
    def __init__(self):
        pygame.init()    
    
        #Creamos la ventana
        screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT),pygame.FULLSCREEN)
        pygame.display.set_caption("NEOSA Es Otro Sistema de Autenticación")
        pygame.mouse.set_visible(0)
        #cargamos los objetos     
        fondo_rand = "fondo" + str(random.randrange(0, 10)) + ".jpg"
        fondo = load_image(fondo_rand, IMG_DIR, False)
        fondo_negro = load_image("fondo_negro.png", IMG_DIR, False)
        alert = Alerta()
        clock = pygame.time.Clock()
        
        time = 300
        time_negro = TIME_BG
        mostrar = False
        
        # el bucle principal
        while True:
            time_negro-=1
            clock.tick(60)        
            
            #Si pasaron 3000 frame, poner fondo negro
            if time_negro <= 0:
                time_negro=0
                screen.blit(fondo_negro,(0,0))
            else:
                #actualizamos la pantalla
                screen.blit(fondo,(0,0))
                #Mostramos las imagenes por 5 seg (en realidad, 300 frame)
                if mostrar:
                    if time == 0:
                        mostrar = False
                    screen.blit(alert.image, alert.rect)
                    time= time-1

            pygame.display.flip()
            
            #posibles entradas del teclado y mouse
            for event in pygame.event.get():
                #Al mover el mouse o teclear alguna tecla, mostramos la información
                if event.type == KEYDOWN or event.type == MOUSEMOTION:
                    time_negro= TIME_BG
                    if (mostrar is not True):
                        mostrar = True
                        time= 300
                #if event.type == KEYDOWN and event.key == K_ESCAPE:
                #    sys.exit()
                #    l.error( "Se intentó acceder al sistema")
                    
                if event.type == pygame.QUIT:
                    l.info( "Se intentó acceder al sistema" )
            
if __name__ == "__main__":
    l.info( "Inhibiendo periféricos de entrada" )
    try:       
        #Eliminamos el proceso o pid file que haya quedado de una ejecución anterior
        pid.kill_process_by_pid("blockhost")
        if(pid.pid_save("blockhost")):
            #se pudo grabar correctamente el PID en el archivo lock
            block_screen()
    except IOError:
        l.error( "No se pudo escribir el PID")
