# -*- coding: utf-8 -*-
class Device_Auth:
      
    def set_device(self, pmac, pname, puuid):
        self.mac = pmac
        self.name = pname
        self.uuidpass = puuid
        
    def __str__(self):
        return "%s (%s)" % (self.name, self.mac)
    
    #Un dispositivo es igual a otro sii mac y uuid son iguales
    def __eq__(self, pmac, puuid):
        if self.mac == pmac:
            if self.uuidpass == puuid:
                return True
        return False
    
    def __getattribute__(self, attr):
        return object.__getattribute__(self, attr)
    