#!/usr/bin/python2
# -*- coding: utf-8 -*-


import sys
import threading
from neosa_icon import NeosaIcon
import os
import subprocess

class StatusThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        icon = NeosaIcon()
        icon.mostrar()
              
if __name__ == "__main__":
    #Iniciamos el servicio en un proceso separado
    subprocess.Popen((sys.executable, os.path.abspath("main/neosa_service.py")))
    #Inicia la aplicación gráfica 
    status = StatusThread()
    status.start()
        